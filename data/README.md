# Database of 250 oxides with stability properties relevant to Water Splitting calculated at PBE and HSE06 levels of theory with FHI-aims code

The database can be directly downloaded and can be queried using instructions provided at https://wiki.fysik.dtu.dk/ase/ase/db/db.html

The properties of oxides are (followed by `_pbe` for PBE and `_hse` for HSE06);

- **e_form:** Formation energy of oxides (eV/atom)
- **e_hull:** Energy above the convex hull (eV/atom) 
- **e_decomp:** Decomposition energy (eV/atom)
- **g_pbx:** Pourbaix free energy at pH=0, V=1.23 (eV/atom) 

## Download

To access the database, follow the steps;

1. Clone the repository: `git clone https://gitlab.com/akhilsnair/asr-workflow.git`
2. Navigate to the project folder: `cd asr-workflow/data`
3. Run `ase db oxides_OER.db`
