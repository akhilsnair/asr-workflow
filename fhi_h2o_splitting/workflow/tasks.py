from ase import Atoms
import os
import warnings
warnings.filterwarnings("ignore")
from pathlib import Path
#ASE terms
from ase import Atoms
from ase.db import connect
from ase.io import read,write, Trajectory
from ase.optimize import BFGS as BFGS
from ase.calculators.aims import Aims,AimsProfile
from ase.build import molecule,bulk
import pytest
from ase import Atoms
from ase.calculators.aims import AimsCube
from ase.optimize import QuasiNewton
from pathlib import Path

species_dir = '/home/askhl/src/secret-dockers/aims/fhi-aims.200112_2/species_defaults/light'
# "/u/akhils/softwares/aims_221103/species_defaults/defaults_2020/tight"
aims_binary = 'aims'
cores = '--ntasks=6'
# aims.221103.scalapack.mpi.x


def aims(parameters):
    profile = AimsProfile(['srun', cores, aims_binary])
    aims_calc = Aims(profile=profile,
                     species_dir=species_dir,
                     **parameters)
    return aims_calc


#Task 1 PBE relaxation
def pbe_relax(atoms, calc):
    from ase.io import read
    calc = aims(calc)
    atoms.calc = calc
    atoms.get_potential_energy()
    return read('aims.out')


#Task 2 HSE SCF calculation
def hse_scf(atoms, calc):
    atoms.calc = aims(calc)
    atoms.get_potential_energy()
    return Path()
