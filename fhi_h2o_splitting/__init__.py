from taskblaster.repository import Repository
from taskblaster.storage import JSONCodec


class ASECodec(JSONCodec):
    def encode(self, obj):
        from ase.io.jsonio import default
        return default(obj)

    def decode(self, dct):
        from ase.io.jsonio import object_hook
        return object_hook(dct)


# Magical "initializer" for taskblaster repository using existing
# asr encoder.  Taskblaster imports this and uses it to initialize
# any repository.
def tb_init_repo(root):
    return Repository(root, usercodec=ASECodec())
