Water Splitting project

Be sure to install taskblaster from

https://gitlab.com/taskblaster/taskblaster

Use "tb init fhi_h2o_splitting" to initialize project

Then workflows and tasks should function correctly and it should be
able to save objects using the ASE JSON encoder.

Do pip e.g. install --editable path fhi-water-splitting/

It's probably wise to install taskblaster from git,
also using --editable, so it's easy to update.
