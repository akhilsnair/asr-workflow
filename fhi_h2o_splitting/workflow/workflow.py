import taskblaster as tb


def node(name, **kwargs):
    return tb.node(f'fhi_h2o_splitting.workflow.tasks.{name}', **kwargs)


def common():
    return {
        'sc_accuracy_rho':'1E-5',
        'sc_accuracy_eev':'1E-3',
        'sc_accuracy_etot':'1E-6',
        'relativistic':"atomic_zora scalar",
        'adjust scf':'always 1'
        'kgrid_density': 5,
    }

def pbe_relax_calc():
    parameters={
        'xc':'pbe',
        'relax_geometry': 'bfgs 1e-3',
        'relax_unit_cell':'.true.'
        **common(),
    }
    return parameters

def hse_scf_calc():
    parameters = {
        'xc':'hse06 0.11',
        'hse_unit':'bohr-1',
        'hybrid_xc_coeff':0.25,
        **common(),
    }
    return parameters


@tb.workflow
class DummyWorkflow:
    atoms = tb.var()

    @tb.task
    def pbe_relaxation(self):
        """
        PBE relaxation : Relax geometry with PBE
        """
        return node('pbe_relax', atoms=self.atoms,
                    calc=pbe_relax_calc())

    @tb.task
    def HSE_scf_calculation(self):
        """
        SCF: SCF calculation taking path from pbe_relax as input
        """
        return node('hse_scf',
                    atoms=self.pbe_relaxation,
                    calc=hse_scf_calc())


@tb.parametrize_glob('*/material')
def workflow(material):
    return DummyWorkflow(atoms=material)
