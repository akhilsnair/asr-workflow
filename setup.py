from setuptools import setup, find_packages


setup(
    name='watersplitting-workflow',
    version='0.0',
    packages=find_packages(),
    python_requires='>=3.8',
    install_requires=['ase'],
    description='code for watersplitting project',
)
